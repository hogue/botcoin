import argparse
import time
import dateparser
import pytz
import json
import pickle
import sys
import random
import csv

from datetime import datetime
from binance.client import Client

# Set parser
parser = argparse.ArgumentParser()
parser.add_argument("--symbol", type=str, help="Symbol", default="ETHBTC")
option = parser.parse_args()

api_key = '5Xjyx4Y53g0QCqRnL22VGurhdi5rGGrjgC41mwd60NT0sGQRJ7tNOMqeD95XKRsj'
api_secret = 'g0zh270R6ohXvT9U1H0AMs5tijBPgFADfJDwaLaqdEfhv0aeDDtdEktmgJtPR0j1'
client = Client(api_key, api_secret)

def unix_time_millis():
    epoch = datetime.utcfromtimestamp(0)
    dt = datetime.utcnow()
    return int((dt - epoch).total_seconds() * 1000.0)

def date_to_milliseconds(date_str):
    """Convert UTC date to milliseconds

    If using offset strings add "UTC" to date string e.g. "now UTC", "11 hours ago UTC"

    See dateparse docs for formats http://dateparser.readthedocs.io/en/latest/

    :param date_str: date in readable format, i.e. "January 01, 2018", "11 hours ago UTC", "now UTC"
    :type date_str: str
    """
    # get epoch value in UTC
    epoch = datetime.utcfromtimestamp(0).replace(tzinfo=pytz.utc)
    # parse our date string
    d = dateparser.parse(date_str)
    # if the date is not timezone aware apply UTC timezone
    if d.tzinfo is None or d.tzinfo.utcoffset(d) is None:
        d = d.replace(tzinfo=pytz.utc)

    # return the difference in time
    return int((d - epoch).total_seconds() * 1000.0)


def interval_to_milliseconds(interval):
    """Convert a Binance interval string to milliseconds

    :param interval: Binance interval string 1m, 3m, 5m, 15m, 30m, 1h, 2h, 4h, 6h, 8h, 12h, 1d, 3d, 1w
    :type interval: str

    :return:
         None if unit not one of m, h, d or w
         None if string not in correct format
         int value of interval in milliseconds
    """
    ms = None
    seconds_per_unit = {
        "m": 60,
        "h": 60 * 60,
        "d": 24 * 60 * 60,
        "w": 7 * 24 * 60 * 60,
        "M": int(4.34524 * 7 * 24 * 60 * 60)
    }

    unit = interval[-1]
    if unit in seconds_per_unit:
        try:
            ms = int(interval[:-1]) * seconds_per_unit[unit] * 1000
        except ValueError:
            pass
    return ms


def get_day_stats(symbol):
    client = Client(api_key, api_secret)

    ticker = client.get_ticker(symbol=symbol)

    return ticker


def get_historical_klines(symbol, interval, start_ts, end_ts, limit=500):
    """Get Historical Klines from Binance

    See dateparse docs for valid start and end string formats http://dateparser.readthedocs.io/en/latest/

    If using offset strings for dates add "UTC" to date string e.g. "now UTC", "11 hours ago UTC"

    :param symbol: Name of symbol pair e.g BNBBTC
    :type symbol: str
    :param interval: Biannce Kline interval
    :type interval: str
    :param start_str: Start date string in UTC format
    :type start_str: str
    :param end_str: optional - end date string in UTC format
    :type end_str: str

    :return: list of OHLCV values

    """
    # create the Binance client, no need for api key
    client = Client("", "")

    # init our list
    output_data = []

    # convert interval to useful value in seconds
    timeframe = interval_to_milliseconds(interval)

    # convert our date strings to milliseconds
    # start_ts = date_to_milliseconds(start_str)

    # if an end time was passed convert it
    #end_ts = None
    #if end_str:
    #    end_ts = date_to_milliseconds(end_str)

    idx = 0
    # it can be difficult to know when a symbol was listed on Binance so allow start time to be before list date
    symbol_existed = False
    while True:
        # fetch the klines from start_ts up to max 500 entries or the end_ts if set
        temp_data = client.get_klines(
            symbol=symbol,
            interval=interval,
            limit=limit,
            startTime=start_ts,
            endTime=None
        )

        #print temp_data, interval, limit, start_ts, end_ts

        if len(temp_data) == 0:
            break

        if len(temp_data) >= limit:
            output_data += temp_data[:limit]
            break

        # handle the case where our start date is before the symbol pair listed on Binance
        if not symbol_existed and len(temp_data):
            symbol_existed = True

        if symbol_existed:
            # append this loops data to our output data
            output_data += temp_data

            # update our start timestamp using the last value in the array and add the interval timeframe
            start_ts = temp_data[len(temp_data) - 1][0] + timeframe
        else:
            # it wasn't listed yet, increment our start date
            start_ts += timeframe

        idx += 1
        # check if we received less than the required limit and exit the loop
        if len(temp_data) < limit:
            # exit the while loop
            break

        # sleep after every 3rd call to be kind to the API
        if idx % 3 == 0:
            time.sleep(1)

    float_data = []
    for val in output_data:
        klines = []
        for kline in val:
            try:
                klines.append(float(kline))
            except Exception as e:
                print val, e
                print 1 / 0
        float_data.append(klines)
    return float_data

def get_pct_diff(current, prev):
    if current == prev or current==0:
        return 0.0
    return ((current-prev) / (current*1.0))

def get_price(symbol):
    ticker = client.get_ticker(symbol=symbol)
    price = float(ticker["askPrice"])
    return price

def normalize(low, high, val):
    if high == low:
        return 1

    return (val-low) / (high-low*1.0)

def g_max(klines, index):

    vals = []
    for kline in klines:
        vals.append(kline[index])
    return max(vals)

def g_min(klines, index):
    vals = []
    for kline in klines:
        vals.append(kline[index])
    return min(vals)

def get_klines_features(symbol, interval, start, ms_per_bucket, BATCH_SIZE):

    end = start
    start = start - ms_per_bucket*BATCH_SIZE

    klines = get_historical_klines(symbol, interval, start, end, limit=BATCH_SIZE)
    if len(klines) != BATCH_SIZE:
        print "10 ERR", len(klines), BATCH_SIZE, symbol
        return None

    # Price indices.
    PRICE_OPEN_INDEX = 1
    PRICE_HIGH_INDEX = 2
    PRICE_LOW_INDEX = 3
    PRICE_CLOSE_INDEX = 4

    VOLUME_INDEX = 5
    QUOTE_ASSET_VOLUME_INDEX = 7
    NUMBER_OF_TRADES_INDEX = 8

    price_max = g_max(klines, PRICE_HIGH_INDEX)
    price_low = g_min(klines, PRICE_LOW_INDEX)

    volume_max = g_max(klines, VOLUME_INDEX)
    volume_min = g_min(klines, VOLUME_INDEX)

    quote_asset_volume_max = g_max(klines, QUOTE_ASSET_VOLUME_INDEX)
    quote_asset_volume_min = g_min(klines, QUOTE_ASSET_VOLUME_INDEX)

    num_trades_max = g_max(klines, NUMBER_OF_TRADES_INDEX)
    num_trades_min = g_min(klines, NUMBER_OF_TRADES_INDEX)
    features = []

    # Append price vector.
    for j in range(BATCH_SIZE):
        features.append(normalize(price_low, price_max, klines[j][PRICE_CLOSE_INDEX]))

    # Append volume vector.
    for j in range(BATCH_SIZE):
        features.append(normalize(volume_min, volume_max, klines[j][VOLUME_INDEX]))

    # Append quote_asset_volume_max vec.
    for j in range(BATCH_SIZE):
        features.append(normalize(quote_asset_volume_min, quote_asset_volume_max, klines[j][QUOTE_ASSET_VOLUME_INDEX]))

     # Append num_trades_min vec.
    for j in range(BATCH_SIZE):
        features.append(normalize(num_trades_min, num_trades_max, klines[j][NUMBER_OF_TRADES_INDEX]))

    # Append quote asset volume raw.
    features.append(klines[j][QUOTE_ASSET_VOLUME_INDEX])

    # Append number of trades raw.
    features.append(klines[j][NUMBER_OF_TRADES_INDEX])

    if len(features) == BATCH_SIZE*4 +2:
        return features

    print "9 ERR", symbol
    return None

def get_tf_features_new(symbol):

    now = unix_time_millis() / 1000
    ms_in_90_days = 7776000000 / 1000

    ms_per_min = 60
    num_batches = [60,40,48,48,48,42,30]
    total_num_buckets = 1 # PRED
    for batch in num_batches:
        total_num_buckets += ((batch*4) + 2)

    # subtract random number from now.
    now -= random.randint(0, ms_in_90_days)
    print now
    
    # Start five mins ago, so we can tell whether we increased within 5 mins or not.
    start = now - (ms_per_min * 5)

    # Past month in 24 hour buckets.
    day_buckets = get_klines_features(symbol, Client.KLINE_INTERVAL_1DAY, start, ms_per_min*60*24, 30)
    if not day_buckets:
        print "2 ERR", symbol
        return None

    # Past hour in minute increments.
    one_min_buckets = get_klines_features(symbol, Client.KLINE_INTERVAL_1MINUTE, start, ms_per_min, 60)
    if not one_min_buckets:
       print "8 ERR", symbol
       return None

    # Past 2 hours in 3 min increments.
    three_min_buckets = get_klines_features(symbol, Client.KLINE_INTERVAL_3MINUTE, start, ms_per_min*3, 40)
    if not three_min_buckets:
        print "7 ERR", symbol
        return None

    # Past 4 hours in 5 min increments.
    five_min_buckets = get_klines_features(symbol, Client.KLINE_INTERVAL_5MINUTE, start, ms_per_min*5, 48)
    if not five_min_buckets:
        print "6 ERR", symbol
        return None

    # Past 12 hours in fifteen min increments.
    fifteen_min_buckets = get_klines_features(symbol, Client.KLINE_INTERVAL_15MINUTE, start, ms_per_min*15, 48)
    if not fifteen_min_buckets:
        print "5 ERR", symbol
        return None

    # Past 48 hours in hour buckets.
    hour_buckets = get_klines_features(symbol, Client.KLINE_INTERVAL_15MINUTE, start, ms_per_min*60, 48)
    if not hour_buckets:
        print "4 ERR", symbol
        return None

    # Past week in 4 hour buckets.
    four_hour_buckets = get_klines_features(symbol, Client.KLINE_INTERVAL_4HOUR, start, ms_per_min*60*4, 42)
    if not four_hour_buckets:
        print "3 ERR", symbol
        return None

    # Get day stats.
    #ticker = get_day_stats(symbol)

    features = one_min_buckets + three_min_buckets + five_min_buckets + fifteen_min_buckets + hour_buckets + four_hour_buckets + day_buckets

    # Volume.
    #features.append(float(ticker['volume']))

    # Count (num trades).
    #features.append(float(ticker['count']))

    # Price change pct.
    #features.append(float(ticker['priceChangePercent']))

    one_min_bucket_truth = get_klines_features(symbol, Client.KLINE_INTERVAL_1MINUTE, now, ms_per_min, 1)


    # Now get the truth.
    price_increased = float(one_min_bucket_truth[0]) >= (one_min_buckets[59] * 1.002)
    if price_increased:
        features.append(1)
    else:
        features.append(0)

    if len(features) == total_num_buckets:
        return features
    print "1", len(features), "!=", total_num_buckets, symbol
    return None

def get_tf_features(symbol):

    now = unix_time_millis()
    ms_per_min = 60000

    
    interval = Client.KLINE_INTERVAL_1MINUTE
    start = now - int(ms_per_min*500)
    klines = get_historical_klines(symbol, interval, start, now)

    BATCH_SIZE = 50

    # Price indices.
    PRICE_OPEN_INDEX = 1
    PRICE_HIGH_INDEX = 2
    PRICE_LOW_INDEX = 3
    PRICE_CLOSE_INDEX = 4

    VOLUME_INDEX = 5
    QUOTE_ASSET_VOLUME_INDEX = 7
    NUMBER_OF_TRADES_INDEX = 8
    #TAKER_BUY_BASE_ASSET_VOLUME_INDEX = 9
    #TAKER_BUY_QUOTE_ASSET_VOLUME_INDEX = 10

    price_max = g_max(klines, PRICE_HIGH_INDEX)
    price_low = g_min(klines, PRICE_LOW_INDEX)

    volume_max = g_max(klines, VOLUME_INDEX)
    volume_min = g_min(klines, VOLUME_INDEX)

    quote_asset_volume_max = g_max(klines, QUOTE_ASSET_VOLUME_INDEX)
    quote_asset_volume_min = g_min(klines, QUOTE_ASSET_VOLUME_INDEX)

    num_trades_max = g_max(klines, NUMBER_OF_TRADES_INDEX)
    num_trades_min = g_min(klines, NUMBER_OF_TRADES_INDEX)

    training_data = []
    for i in range(0, len(klines)-((BATCH_SIZE*2)-1),BATCH_SIZE):
        features = []

        # Append price vector.
        for j in range(BATCH_SIZE):
            features.append(normalize(price_low, price_max, klines[i][PRICE_CLOSE_INDEX]))

        # Append volume vector.
        for j in range(BATCH_SIZE):
            features.append(normalize(volume_min, volume_max, klines[i][VOLUME_INDEX]))

        # Append quote_asset_volume_max vec.
        for j in range(BATCH_SIZE):
            features.append(normalize(quote_asset_volume_min, quote_asset_volume_max, klines[i][QUOTE_ASSET_VOLUME_INDEX]))

         # Append num_trades_min vec.
        for j in range(BATCH_SIZE):
            features.append(normalize(num_trades_min, num_trades_max, klines[i][NUMBER_OF_TRADES_INDEX]))

        # Append quote asset volume raw.
        features.append(klines[i][QUOTE_ASSET_VOLUME_INDEX])

        # Append number of trades raw.
        features.append(klines[i][NUMBER_OF_TRADES_INDEX])

        # Get day stats.
        ticker = get_day_stats(symbol)

        # Volume.
        features.append(float(ticker['volume']))

        # Count (num trades).
        features.append(float(ticker['count']))

        # Price change pct.
        features.append(float(ticker['priceChangePercent']))

        # Now put the truth.
        price_current = klines[i+BATCH_SIZE-1][PRICE_CLOSE_INDEX]
        price_later = klines[i+BATCH_SIZE][PRICE_HIGH_INDEX]

        # Binary classification.
        if price_later >= (price_current * 1.002):
            features.append(1)
        else:
            features.append(0)

        if len(features) == 206:
            training_data.append(features)

    return training_data

def main():

    symbol = option.symbol

    
    training = get_tf_features_new(symbol)
    if training:
        choice = random.choice([0,1,2,3,4,5,6,7,8,9])
        if choice < 3:
            fname = 'train/with_other_dims_test.data'
        else:
            fname = 'train/with_other_dims_train.data'
        
        with open(fname, "a") as file:
            writer = csv.writer(file, delimiter=',')
            writer.writerow(training)
    print "Done with", symbol

if __name__ == "__main__":
    main()