# Copyright 2017 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Example code for TensorFlow Wide & Deep Tutorial using tf.estimator API."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import shutil
import sys

import tensorflow as tf

#_CSV_COLUMNS = [
#    'age', 'workclass', 'fnlwgt', 'education', 'education_num',
#    'marital_status', 'occupation', 'relationship', 'race', 'gender',
#    'capital_gain', 'capital_loss', 'hours_per_week', 'native_country',
#    'income_bracket'
#]

_CSV_COLUMNS = [
  'price_ts',
  'volume_ts',
  'quote_asset_volume_ts',
  'num_trades_ts',
  'quote_asset_volume',
  'num_trades',
  'volume',
  'count' ,
  'price_change_pct',
]

_CSV_COLUMN_DEFAULTS_OLD = [[0.]] * 206

_CSV_COLUMN_DEFAULTS = [[0.]] * 1279


#_CSV_COLUMN_DEFAULTS = [[0], [''], [0], [''], [0], [''], [''], [''], [''], [''],
#                        [0], [0], [0], [''], ['']]

parser = argparse.ArgumentParser()

parser.add_argument(
    '--model_dir', type=str, default='/tmp/day_trade_90day',
    help='Base directory for the model.')

parser.add_argument(
    '--model_type', type=str, default='deep',
    help="Valid model types: {'wide', 'deep', 'wide_deep'}.")

parser.add_argument(
    '--train_epochs', type=int, default=400, help='Number of training epochs.')

parser.add_argument(
    '--epochs_per_eval', type=int, default=1,
    help='The number of training epochs to run between evaluations.')

parser.add_argument(
    '--batch_size', type=int, default=100, help='Number of examples per batch.')

parser.add_argument(
    '--train_data', type=str, default='train/train_2',
    help='Path to the training data.')

parser.add_argument(
    '--test_data', type=str, default='train/test_2',
    help='Path to the test data.')

_NUM_EXAMPLES = {
    'train': 12372,
    'validation': 5307,
}

def build_model_columns_old():
  """Builds a set of wide and deep feature columns."""
  # Continuous columns
  
  price_ts = tf.feature_column.numeric_column('price_ts', shape=(60,))
  volume_ts = tf.feature_column.numeric_column('volume_ts', shape=(60,))
  quote_asset_volume_ts = tf.feature_column.numeric_column('quote_asset_volume_ts', shape=(60,))
  num_trades_ts = tf.feature_column.numeric_column('num_trades_ts', shape=(60,))
  
  quote_asset_volume = tf.feature_column.numeric_column('quote_asset_volume')
  num_trades = tf.feature_column.numeric_column('num_trades')
  volume = tf.feature_column.numeric_column('volume')
  count = tf.feature_column.numeric_column('count')
  price_change_pct = tf.feature_column.numeric_column('price_change_pct')
  label = tf.feature_column.numeric_column('label')

  base_columns = [
      price_ts, volume_ts, quote_asset_volume_ts, num_trades_ts,
      quote_asset_volume, num_trades, volume, count, price_change_pct,
  ]

  
  crossed_columns = [
      tf.feature_column.crossed_column(
          ['price_ts', 'volume_ts'], hash_bucket_size=1000),
      tf.feature_column.crossed_column(
          ['num_trades_ts', 'volume_ts'], hash_bucket_size=1000),
  ]

  #base_columns += crossed_columns

  return base_columns, None


def build_model_columns():
  """Builds a set of wide and deep feature columns."""
  # Continuous columns

  configs = [
    ('one_min', 60),
    ('three_min', 40),
    ('five_min', 48),
    ('fifteen_min', 48),
    ('hour', 48),
    ('four_hour', 42),
    ('daily', 30)
  ]

  base_columns = []

  for config in configs:
    cfg_name, cfg_shape = config
    price_ts = tf.feature_column.numeric_column('price_ts' + cfg_name, shape=(cfg_shape,))
    volume_ts = tf.feature_column.numeric_column('volume_ts' + cfg_name, shape=(cfg_shape,))
    quote_asset_volume_ts = tf.feature_column.numeric_column('quote_asset_volume_ts' + cfg_name, shape=(cfg_shape,))
    num_trades_ts = tf.feature_column.numeric_column('num_trades_ts' + cfg_name, shape=(cfg_shape,))
    quote_asset_volume = tf.feature_column.numeric_column('quote_asset_volume' + cfg_name)
    num_trades = tf.feature_column.numeric_column('num_trades' + cfg_name)

    cfg_columns = [
      price_ts, volume_ts, quote_asset_volume_ts, num_trades_ts,
      quote_asset_volume, num_trades,
    ]

    for col in cfg_columns:
      base_columns.append(col)

  label = tf.feature_column.numeric_column('label')

  base_columns.append(label)

  
  crossed_columns = [
      tf.feature_column.crossed_column(
          ['price_ts', 'volume_ts'], hash_bucket_size=1000),
      tf.feature_column.crossed_column(
          ['num_trades_ts', 'volume_ts'], hash_bucket_size=1000),
  ]

  #base_columns += crossed_columns

  return base_columns, None


def build_estimator(model_dir, model_type):
  """Build an estimator appropriate for the given model type."""
  wide_columns, deep_columns = build_model_columns()
  hidden_units = [111, 7, 15, 198, 137, 60, 142, 170, 83, 16, 43, 99, 182, 182]

  # Create a tf.estimator.RunConfig to ensure the model is run on CPU, which
  # trains faster than GPU for this model.
  run_config = tf.estimator.RunConfig().replace(
      session_config=tf.ConfigProto(device_count={'GPU': 0}))

  if model_type == 'wide':
    return tf.estimator.LinearClassifier(
        model_dir=model_dir,
        feature_columns=wide_columns,
        config=run_config)
  elif model_type == 'deep':
    return tf.estimator.DNNClassifier(
        model_dir=model_dir,
        optimizer=tf.train.AdamOptimizer(learning_rate=0.3),
        feature_columns=wide_columns,
        hidden_units=hidden_units,
        config=run_config)
  else:
    return tf.estimator.DNNLinearCombinedClassifier(
        model_dir=model_dir,
        linear_feature_columns=wide_columns,
        dnn_feature_columns=deep_columns,
        dnn_hidden_units=hidden_units,
        config=run_config)


def input_fn(data_file, num_epochs, shuffle, batch_size):
  """Generate an input function for the Estimator."""
  assert tf.gfile.Exists(data_file), (
      '%s not found. Please make sure you have either run data_download.py or '
      'set both arguments --train_data and --test_data.' % data_file)

  def parse_csv_old(value):
    print('Parsing', data_file)
    columns = tf.decode_csv(value, record_defaults=_CSV_COLUMN_DEFAULTS)
    print("XXX")

    price_ts = tf.stack(columns[:50])
    volume_ts = tf.stack(columns[50:100])
    quote_asset_volume_ts = tf.stack(columns[100:150])
    num_trades_ts = tf.stack(columns[150:200])

    quote_asset_volume = columns[200]
    num_trades = columns[201]
    volume = columns[202]
    count = columns[203]
    price_change_pct = columns[204]
    labels = columns[205]

    features = {
      'price_ts' : price_ts,
      'volume_ts': volume_ts,
      'quote_asset_volume_ts': quote_asset_volume_ts,
      'num_trades_ts': num_trades_ts,
      'quote_asset_volume': quote_asset_volume,
      'num_trades': num_trades,
      'volume': volume,
      'count' : count,
      'price_change_pct': price_change_pct,
      'label': labels
    }
    return features, tf.equal(labels, 1)

  def parse_csv(value):
    print('Parsing', data_file)
    columns = tf.decode_csv(value, record_defaults=_CSV_COLUMN_DEFAULTS)
    print("XXX")

    configs = [
      ('one_min', 60),
      ('three_min', 40),
      ('five_min', 48),
      ('fifteen_min', 48),
      ('hour', 48),
      ('four_hour', 42),
      ('daily', 30)
    ]

    base_columns = []

    index = 0
    features = dict()
    for config in configs:
      cfg_name, cfg_shape = config

      price_ts = tf.stack(columns[index:index+cfg_shape])
      volume_ts = tf.stack(columns[index+cfg_shape:index+cfg_shape*2])
      quote_asset_volume_ts = tf.stack(columns[index+cfg_shape*2:index+cfg_shape*3])
      num_trades_ts = tf.stack(columns[index+cfg_shape*3:index+cfg_shape*4])

      quote_asset_volume = columns[index+cfg_shape*4]
      num_trades = columns[index+cfg_shape*4+1]

      features['price_ts' + cfg_name] = price_ts
      features['volume_ts' + cfg_name] = volume_ts
      features['quote_asset_volume_ts' + cfg_name] = quote_asset_volume_ts
      features['num_trades_ts' + cfg_name] = num_trades_ts
      features['quote_asset_volume' + cfg_name] = quote_asset_volume
      features['num_trades' + cfg_name] = num_trades

      index += cfg_shape * 4 + 2
    
    labels = columns[-1]
    features['label'] = labels
    return features, tf.equal(labels, 1)

  # Extract lines from input files using the Dataset API.
  dataset = tf.data.TextLineDataset(data_file)

  if shuffle:
    dataset = dataset.shuffle(buffer_size=_NUM_EXAMPLES['train'])

  dataset = dataset.map(parse_csv, num_parallel_calls=5)
  print("ZZZ DEDA")

  # We call repeat after shuffling, rather than before, to prevent separate
  # epochs from blending together.
  dataset = dataset.repeat(num_epochs)
  dataset = dataset.batch(batch_size)

  iterator = dataset.make_one_shot_iterator()
  features, labels = iterator.get_next()
  return features, labels


def main(unused_argv):
  # Clean up the model directory if present
  shutil.rmtree(FLAGS.model_dir, ignore_errors=True)
  model = build_estimator(FLAGS.model_dir, FLAGS.model_type)

  # Train and evaluate the model every `FLAGS.epochs_per_eval` epochs.
  for n in range(FLAGS.train_epochs // FLAGS.epochs_per_eval):
    model.train(input_fn=lambda: input_fn(
        FLAGS.train_data, FLAGS.epochs_per_eval, True, FLAGS.batch_size))

    results = model.evaluate(input_fn=lambda: input_fn(
        FLAGS.test_data, 1, False, FLAGS.batch_size))

    # Display evaluation metrics
    print('Results at epoch', (n + 1) * FLAGS.epochs_per_eval)
    print('-' * 60)

    for key in sorted(results):
      print('%s: %s' % (key, results[key]))


if __name__ == '__main__':
  tf.logging.set_verbosity(tf.logging.INFO)
  FLAGS, unparsed = parser.parse_known_args()
  tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)