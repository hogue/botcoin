sanitized_lines = []
with open("train/with_other_dims_train.data", "r") as f:
     for line in f:
         if line.count(',') == 1278: # 205:
             sanitized_lines.append(line)

with open("train/train_2", "w") as f:
     for line in sanitized_lines:
         f.write(line)

print(len(sanitized_lines))

sanitized_lines = []
with open("train/with_other_dims_test.data", "r") as f:
     for line in f:
         if line.count(',') == 1278:
             sanitized_lines.append(line)

with open("train/test_2", "w") as f:
     for line in sanitized_lines:
         f.write(line)

print(len(sanitized_lines))
