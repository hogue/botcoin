import time
import dateparser
import pytz
import json
import pickle
import os
import subprocess
import sys 
import random
import time

from subprocess import call
from datetime import datetime
from binance.client import Client
from multiprocessing.dummy import Pool as ThreadPool 

api_key = '5Xjyx4Y53g0QCqRnL22VGurhdi5rGGrjgC41mwd60NT0sGQRJ7tNOMqeD95XKRsj'
api_secret = 'g0zh270R6ohXvT9U1H0AMs5tijBPgFADfJDwaLaqdEfhv0aeDDtdEktmgJtPR0j1'
client = Client(api_key, api_secret)

def get_all_symbols():
    symbols = []

    # get all symbol prices
    prices = client.get_all_tickers()

    for price in prices:
        symbols.append(price['symbol'])

    return symbols

def run_python(symbol):
    subprocess.Popen([sys.executable, '/Users/michael/Downloads/python-binance-master/tf/get_klines.py',  '--symbol=' + symbol])
    return None

def main():

    # Create our hard list of training coins. We will NOT use any of these for testing!!!!
    symbols = get_all_symbols()

    while True:
        try:

            # Get k random things from symbols.
            random_symbols = set()
            while len(random_symbols) < 10:
                random_symbols.add(random.choice(symbols))

            pool = ThreadPool(10)
            results = pool.map(run_python, random_symbols)
            pool.close()
            pool.join()
            time.sleep(1)

        except Exception as e:
            print e

if __name__ == "__main__":
    main()